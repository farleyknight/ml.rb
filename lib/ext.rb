
require 'gsl'

include GSL

Vector.class_eval do
  def self.ones(n)
    Vector.alloc(n).set_all(1)
  end

  def self.zeroes(n)
    Vector.alloc(n).set_all(0)
  end
end
