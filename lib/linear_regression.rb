class LinearRegression
  # Parameters:
  #
  # x - m by n matrix
  # y - vector of length m
  #
  # TODO (farleyknight@gmail.com): If x happens to be a vector
  # we should use the, presumably, less costly computation of
  # using GSL::Fit::linear(x, y) over GSL::MultiFit::linear(X, y)
  def fit(x, y)
    raise "" unless x.is_a? Matrix
    raise "" unless y.is_a? Vector and y.length == x.shape[1]
  end

  # x - vector of length n
  def predict(x)

  end
end
