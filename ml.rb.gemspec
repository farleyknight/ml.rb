
Gem::Specification.new do |s|
  s.name        = 'ml.rb'
  s.version     = '0.0.1'
  s.date        = '2012-08-14'
  s.summary     = "A bunch of machine learning algorithms, written in Ruby"
  s.description = "A bunch of machine learning algorithms, written in Ruby"
  s.authors     = ["Farley Knight"]
  s.email       = 'farleyknight@gmail.com'

  s.homepage    = 'https://github.com/farleyknight/ml.rb'

  s.add_development_dependency "bundler", ">= 1.0.0"

  s.files        = `git ls-files`.split("\n")
  s.executables  = `git ls-files`.split("\n").map{|f| f =~ /^bin\/(.*)/ ? $1 : nil}.compact
  s.require_path = 'lib'
end

